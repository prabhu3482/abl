package main

import (
	"testing"
	"gopkg.in/go-resty/resty.v2"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
)

func TestAPIStatus(t *testing.T) {
    client := resty.New()
    resp, _ := client.R().Get("http://localhost:8080/testapi")
    fmt.Printf("\nResponse Body: %v\n", resp)
    assert.Equal(t, 200, resp.StatusCode())
    body := `Hello Go routine !!! 
 Hello Bye !!!`
    assert.Equal(t, body, string(resp.Body()))

}

type StatusResponse struct {
    Status string `json:"status"`
}

func TestGetQuantity(t *testing.T) {
    client := resty.New()
    resp, _ := client.R().Get("http://localhost:8080/getquantity?prd_id=1000")
    if resp.StatusCode() != 200 {
        t.Errorf("Unexpected status code, expected %d, got %d instead", 200, resp.StatusCode())
    }
    fmt.Printf("\nResponse Body: %v\n", resp)
    myResponse := StatusResponse{}
    err := json.Unmarshal(resp.Body(), &myResponse)
    if err != nil {
        fmt.Println(err)
        return
    }
    assert.Equal(t, "success", myResponse.Status)
}

func TestAddToCart(t *testing.T) {
    client := resty.New()
    resp, _ := client.R().Get("http://localhost:8080/addcart?prd_id=1000&user_id=1&quantity=1&price=150")
    if resp.StatusCode() != 200 {
        t.Errorf("Unexpected status code, expected %d, got %d instead", 200, resp.StatusCode())
    }
    fmt.Printf("\nResponse Body: %v\n", resp)
    myResponse := StatusResponse{}
    err := json.Unmarshal(resp.Body(), &myResponse)
    if err != nil {
        fmt.Println(err)
        return
    }
    assert.Equal(t, "success", myResponse.Status)
}

func TestCheckout(t *testing.T) {
    client := resty.New()
    resp, _ := client.R().Get("http://localhost:8080/assignproduct?user_id=1")
    if resp.StatusCode() != 200 {
        t.Errorf("Unexpected status code, expected %d, got %d instead", 200, resp.StatusCode())
    }
    fmt.Printf("\nResponse Body: %v\n", resp)
    myResponse := StatusResponse{}
    err := json.Unmarshal(resp.Body(), &myResponse)
    if err != nil {
        fmt.Println(err)
        return
    }
    assert.Equal(t, "success", myResponse.Status)
}

