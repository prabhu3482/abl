package utils

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	"net/url"
	"os"
	"reflect"
	"runtime"
	"strconv"
	"strings"
    "os/signal"
    "syscall"
	"regexp"

	Config "../config"

	uuid "github.com/satori/go.uuid"
	"github.com/valyala/fasthttp"

	log "github.com/Sirupsen/logrus"
	"github.com/natefinch/lumberjack"
)

//LogFormatter log formatter structure
type LogFormatter struct {
	TimestampFormat string
	LevelDesc       []string
}

func (f *LogFormatter) Format(entry *log.Entry) ([]byte, error) {
	timestamp := fmt.Sprintf(entry.Time.Format(f.TimestampFormat))

	if f.LevelDesc[entry.Level] == "ERROR" || f.LevelDesc[entry.Level] == "PANIC" || f.LevelDesc[entry.Level] == "FATAL" {

		if pc, file, line, ok := runtime.Caller(1); ok {
			funcName := runtime.FuncForPC(pc).Name()
			return []byte(fmt.Sprintf("%s %s [%s %v %s] %s\n", timestamp, f.LevelDesc[entry.Level], file, line, funcName, entry.Message)), nil
		}

	}
	return []byte(fmt.Sprintf("%s %s %s\n", timestamp, f.LevelDesc[entry.Level], entry.Message)), nil
}

func init() {

	logFormatter := new(LogFormatter)
	logFormatter.TimestampFormat = "2006-01-02 15:04:05.000"
	logFormatter.LevelDesc = []string{"PANIC", "FATAL", "ERROR", "WARN", "INFO", "DEBUG", "TRACE"}

	log.SetFormatter(logFormatter)
	//log.SetReportCaller(true)

	level, err := log.ParseLevel(Config.LOGLEVEL)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	log.SetLevel(level)

	log.SetOutput(&lumberjack.Logger{
		Filename:   Config.LOGFILE,
		MaxSize:    Config.LOGFILEMAXSIZE,
		MaxBackups: Config.LOGFILEMAXBACKUP,
		MaxAge:     Config.LOGFILEMAXAGE,
		Compress:   true,
		LocalTime:  true,
	})
	log.Infof("Starting Enqueuer Service")
}

//CreatePidFile generic utility to create a file with current pid
func CreatePidFile(pidfile string) {
	err := ioutil.WriteFile(pidfile, []byte(fmt.Sprintf("%d", os.Getpid())), 0644)
	if err != nil {
		panic(err)
	}
}


//catchSignals just catched all INT TERM etc signals and exits after removing pidfile
func CatchSignals(PIDFILE string) {
        signalChan := make(chan os.Signal, 1)
        signal.Notify(signalChan, syscall.SIGHUP,
                syscall.SIGINT,
                syscall.SIGTERM,
                syscall.SIGQUIT)
        go func() {
                sig := <-signalChan
                log.Printf("GOT SIGNAL %v", sig)
                //stopflag = true
                ExitProg("All Done", PIDFILE)
        }()

}

//ExitProg Function is for removing pidfile on exit
func ExitProg(msg string, pidfile string) {
	log.Printf("Exiting goserver %s", msg)
	os.Remove(pidfile)
	os.Exit(0)
}

//GetUUID generates a new UUID
func GetUUID() string {
	UUID := uuid.Must(uuid.NewV4())
	return UUID.String()
}

//GetURLParams convert url string data into map
func GetURLParams(ctx *fasthttp.RequestCtx, data string) map[string]interface{} {
	var queryParams = make(map[string]interface{})
	ss := strings.Split(data, "&")
	for _, pair := range ss {
		z := strings.Split(pair, "=")
		if len(z) == 2 {
			queryParams[z[0]], _ = url.PathUnescape(z[1])
		}
	}
	return queryParams
}

func GetPostData(ctx *fasthttp.RequestCtx) map[string]interface{} {
        body := string(ctx.PostBody())

        r := regexp.MustCompile(`(?s).*?(\{.*\})`)
        raw := r.FindStringSubmatch(body)
        var json_data map[string]interface{}
	if len(raw) > 0 {
		json_str := raw[1]
                json.Unmarshal([]byte(json_str), &json_data)
	}
        return json_data
}

func GetPostParam(ctx *fasthttp.RequestCtx, key string) string {
	return string(ctx.FormValue(key))
}

//StringToINT32 convert string to int32
func StringToINT32(input string) int32 {
	intvalue, err := strconv.ParseInt(input, 10, 64)
	if err != nil {
		return 0
	}
	return int32(intvalue)
}

//StringToINT convert string to int
func StringToINT(input string) int {
        intvalue, err := strconv.Atoi(input)
        if err != nil {
                return 0
        }
        return int(intvalue)
}

func StringToFloat64(input string) float64 {
        intvalue, err := strconv.ParseFloat(input, 64)
        if err != nil {
                return 0
        }
        return float64(intvalue)
}


//StringArrayToINT convert array of sting to array of int
func StringArrayToINT(input []string) []int {
	var intarray = []int{}
	for _, i := range input {
		j, err := strconv.Atoi(i)
		if err != nil {
			panic(err)
		}
		intarray = append(intarray, j)
	}
	return intarray
}

//Floattostrwithprec return string
func Floattostrwithprec(fv float64, prec int) string {
	return strconv.FormatFloat(fv, 'f', prec, 64)
}


//ParseJSONStringToMap parse json striong to map
func ParseJSONStringToMap(Jsonstr string) (map[string]interface{}, bool) {

	var result map[string]interface{}
	err := json.Unmarshal([]byte(Jsonstr), &result)
	if err != nil {
		return result, false
	}
	return result, true
}

//GetInterfaceSlice return length of interface
func GetInterfaceSlice(slice interface{}) []interface{} {
	s := reflect.ValueOf(slice)
	if s.Kind() != reflect.Slice {
		log.Printf("InterfaceSlice() given a non-slice type")
	}
	ret := make([]interface{}, s.Len())
	for i := 0; i < s.Len(); i++ {
		ret[i] = s.Index(i).Interface()
	}
	return ret
}

//GetInterface return length of interface
func GetInterface(slice interface{}) map[string]interface{} {
	v := reflect.ValueOf(slice)
	var data = map[string]interface{}{}

	if v.Kind() == reflect.Map {
		for _, key := range v.MapKeys() {
			strct := v.MapIndex(key)
			//log.Println(key.Interface(), strct.Interface())
			data[(key.Interface()).(string)] = strct.Interface()
		}
	}
	return data
}

//GetInterfaceIntSlice return length of interface
func GetInterfaceIntSlice(slice interface{}) []int32 {
	s := reflect.ValueOf(slice)
	if s.Kind() != reflect.Slice {
		log.Printf("InterfaceSlice() given a non-slice type")
	}
	ret := make([]int32, s.Len())
	for i := 0; i < s.Len(); i++ {
		ret[i] = (s.Index(i).Interface()).(int32)
	}
	return ret
}
