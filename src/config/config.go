package config

const (
	//HTTPPORT The port where go server is running, type string
	HTTPPORT = ":8080"

	//AREDISHOST redis host to push data
	AREDISHOST = "127.0.0.1:6379"

	LOGFILE          = "/tmp/apiserver.log"
	LOGLEVEL         = "INFO"
	LOGFILEMAXSIZE   = 500
	LOGFILEMAXAGE    = 30
	LOGFILEMAXBACKUP = 500

	//PIDFILE location where the pid file exists and also for lock
	PIDFILE = "/tmp/abl_api.pid"

	//BUFFERLIMIT max buffer limit
	BUFFERLIMIT = 100

	MAXRETRY = 5

	// DEFAULTMSG is the message sent to all wrong urls
	DEFAULTMSG = "Custom http page not found"

	MONGODB = "mongodb://localhost:27017/"
)
