package main

/*
* This is the main server code
* We start the http server here
* See the Makefile and Readme for more details
 */

import (
	"fmt"

	"time"
	"./utils"
	"./config"
	log "github.com/Sirupsen/logrus"
	"github.com/valyala/fasthttp"
)

var err error

var start time.Time
var stopflag bool

func init() {
	utils.CatchSignals(config.PIDFILE)
	utils.CreatePidFile(config.PIDFILE)
	stopflag = false
}

func startHTTP(httpport string) {
	s := &fasthttp.Server{
		Handler: fastHTTPHandler,
		Name:    "Custom HTTP",
	}
	err = s.ListenAndServe(httpport)
	if err != nil {
		fmt.Println(err)
		log.Print(err)
		log.Fatalf("Could not Start http server  at %s", config.HTTPPORT)
	}
}

/* main function */
func main() {
	startHTTP(config.HTTPPORT)
}

