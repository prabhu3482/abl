package endpoints

import (
	"encoding/json"
	"fmt"
	"time"

	Process "../processor"
	Utils "../utils"
	log "github.com/Sirupsen/logrus"
	"github.com/valyala/fasthttp"
)

func Testapi(ctx *fasthttp.RequestCtx) {
	ctx.Write([]byte("Hello Go routine !!! \n "))
	var queryParams = make(map[string]interface{})
	queryParams = Utils.GetURLParams(ctx, (ctx.QueryArgs()).String())
	log.Printf("ReqId :%v Get Params:  %+v", ctx.UserValue("reqID"), queryParams)
	go Process.Testgo(queryParams, ctx.UserValue("reqID"))
	ctx.Write([]byte("Hello Bye !!!"))
	ctx.SetStatusCode(200)

}

func GetQuantity(ctx *fasthttp.RequestCtx) {
	var queryParams = make(map[string]interface{})
	var response = make(map[string]interface{})
	st := time.Now()
	ReqIP := fmt.Sprintf("%s", ctx.RemoteIP())
	queryParams = Utils.GetURLParams(ctx, (ctx.QueryArgs()).String())
	queryParams["ReqIP"] = ReqIP
	log.Printf("ReqId :%v Get Params:  %+v", ctx.UserValue("reqID"), queryParams)
	resp_data, err := Process.GetProductQuantity(ctx.UserValue("reqID").(string), queryParams)
	if err != nil {
		response["status"] = "failed"
		response["description"] = err.Error()
	} else {
		response["status"] = "success"
		response["quantity"] = resp_data
	}
	log.Printf("ReqId :%v TimeTeken:%v", ctx.UserValue("reqID"), time.Now().Sub(st))
	jsonString, _ := json.Marshal(response)
	ctx.Write([]byte(jsonString))
	ctx.SetStatusCode(200)
	return
}

func AddCart(ctx *fasthttp.RequestCtx) {
	var queryParams = make(map[string]interface{})
	var response = make(map[string]interface{})
	st := time.Now()
	ReqIP := fmt.Sprintf("%s", ctx.RemoteIP())
	queryParams = Utils.GetURLParams(ctx, (ctx.QueryArgs()).String())
	queryParams["ReqIP"] = ReqIP
	log.Printf("ReqId :%v Get Params:  %+v", ctx.UserValue("reqID"), queryParams)
	resp_data, err := Process.AddToCart(ctx.UserValue("reqID").(string), queryParams)
	if err != nil {
		response["status"] = "failed"
		response["description"] = err.Error()
	} else {
		response["status"] = "success"
		response["description"] = resp_data
	}
	log.Printf("ReqId :%v TimeTeken:%v", ctx.UserValue("reqID"), time.Now().Sub(st))
	jsonString, _ := json.Marshal(response)
	ctx.Write([]byte(jsonString))
	ctx.SetStatusCode(200)
	return
}

func AssignProduct(ctx *fasthttp.RequestCtx) {
	var queryParams = make(map[string]interface{})
	var response = make(map[string]interface{})
	st := time.Now()
	ReqIP := fmt.Sprintf("%s", ctx.RemoteIP())
	queryParams = Utils.GetURLParams(ctx, (ctx.QueryArgs()).String())
	queryParams["ReqIP"] = ReqIP
	log.Printf("ReqId :%v Get Params:  %+v", ctx.UserValue("reqID"), queryParams)
	resp_data, err := Process.AssignProducts(ctx.UserValue("reqID").(string), queryParams)
	if err != nil {
		response["status"] = "failed"
		response["description"] = err.Error()
	} else {
		response["status"] = "success"
		response["description"] = resp_data
	}
	log.Printf("ReqId :%v TimeTeken:%v", ctx.UserValue("reqID"), time.Now().Sub(st))
	jsonString, _ := json.Marshal(response)
	ctx.Write([]byte(jsonString))
	ctx.SetStatusCode(200)
	return
}
