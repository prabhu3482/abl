package processor

import (
	"time"

	log "github.com/Sirupsen/logrus"
)

func Testgo(queryParams map[string]interface{}, UUID interface{}) {
	time.Sleep(1 * time.Millisecond)
	log.Printf("ReqID :%v TIME : %v", UUID, time.Now())
	return
}
