package processor

import (
	"errors"
	"fmt"
	"strconv"

	MongoDB "../mongodb"
	log "github.com/Sirupsen/logrus"
)

func GetProductQuantity(UUID string, queryParams map[string]interface{}) (string, error) {
	log.Printf("ReqID :%v param: %v", UUID, queryParams)
	var response string
	var err error
	var prd_id int
	switch v := queryParams["prd_id"].(type) {
	case string:
		prd_id, _ = strconv.Atoi(queryParams["prd_id"].(string))
	case float64:
		prd_id = int(v)
	default:
		return response, errors.New("product id param invalid type. Expecting in integer param ")
	}
	quantity, err1 := MongoDB.GetProductQtyDetails(UUID, prd_id)
	if err1 != nil {
		return response, err1
	}
	response = fmt.Sprintf("%f", quantity)

	return response, err
}
