package processor

import (
	"errors"
	"strconv"

	MongoDB "../mongodb"
	log "github.com/Sirupsen/logrus"
)

func AddToCart(UUID string, queryParams map[string]interface{}) (string, error) {
	log.Printf("ReqID :%v param: %v", UUID, queryParams)
	var response string
	var err error
	var prd_id int
	var user_id int
	var quantity int
	var price int
	switch v := queryParams["prd_id"].(type) {
	case string:
		prd_id, _ = strconv.Atoi(queryParams["prd_id"].(string))
	case float64:
		prd_id = int(v)
	default:
		return response, errors.New("product id param invalid type. Expecting in integer param ")
	}
	switch v := queryParams["user_id"].(type) {
	case string:
		user_id, _ = strconv.Atoi(queryParams["user_id"].(string))
	case float64:
		user_id = int(v)
	default:
		return response, errors.New("user id param invalid type. Expecting in integer param ")
	}
	switch v := queryParams["quantity"].(type) {
	case string:
		quantity, _ = strconv.Atoi(queryParams["quantity"].(string))
	case float64:
		quantity = int(v)
	default:
		return response, errors.New("quantity param invalid type. Expecting in integer param ")
	}
	switch v := queryParams["price"].(type) {
	case string:
		price, _ = strconv.Atoi(queryParams["price"].(string))
	case float64:
		price = int(v)
	default:
		return response, errors.New("price param invalid type. Expecting in integer param ")
	}
	status, err1 := MongoDB.AddToCartcolection(UUID, prd_id, user_id, quantity, price)
	if err1 != nil {
		return response, err1
	}
	response = status

	return response, err
}
