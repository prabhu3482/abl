package processor

import (
	"errors"
	"strconv"

	MongoDB "../mongodb"
	log "github.com/Sirupsen/logrus"
)

func AssignProducts(UUID string, queryParams map[string]interface{}) (string, error) {
	log.Printf("ReqID :%v param: %v", UUID, queryParams)
	var response string
	var err error
	var user_id int

	switch v := queryParams["user_id"].(type) {
	case string:
		user_id, _ = strconv.Atoi(queryParams["user_id"].(string))
	case float64:
		user_id = int(v)
	default:
		return response, errors.New("user id param invalid type. Expecting in integer param ")
	}

	status, err1 := MongoDB.CheckoutCart(UUID, user_id)
	if err1 != nil {
		return response, err1
	}
	response = status

	return response, err
}
