package main

import (
	"fmt"

	log "github.com/Sirupsen/logrus"

	"./config"
	"./endpoints"
	"./utils"

	"github.com/valyala/fasthttp"
)

func notFound(ctx *fasthttp.RequestCtx) {
	fmt.Fprintln(ctx, config.DEFAULTMSG)
}

func fastHTTPHandler(ctx *fasthttp.RequestCtx) {
	ctx.Response.Header.Set("Access-Control-Allow-Origin", "*")
	UUID := utils.GetUUID()
	ctx.SetUserValue("reqID", UUID)

	log.Print("ReqId :", (ctx.UserValue("reqID")), " IP:", ctx.RemoteIP(), " - ", string(ctx.URI().RequestURI()))
	switch string(ctx.Path()) {
	case "/getquantity":
		endpoints.GetQuantity(ctx)
	case "/addcart":
		endpoints.AddCart(ctx)
	case "/assignproduct":
		endpoints.AssignProduct(ctx)
	case "/testapi":
		endpoints.Testapi(ctx)
	default:
		notFound(ctx)
	}
}
