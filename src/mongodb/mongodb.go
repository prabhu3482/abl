package mongodb

import (
	"context"
	"errors"
	"time"

	Config "../config"
	log "github.com/Sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var mongodb *mongo.Client

func init() {
	ctx := context.Background()
	var err error
	clientOpts := options.Client().ApplyURI(Config.MONGODB)
	mongodb, err = mongo.Connect(ctx, clientOpts)
	if err != nil {
		panic(err)
	}

}

func GetProductQtyDetails(UUID string, prd_id int) (float64, error) {
	collection := mongodb.Database("abl").Collection("products")

	filter := bson.D{{Key: "_id", Value: prd_id}}
	cur, err := collection.Find(context.Background(), filter)
	if err != nil {
		log.Fatal(err)
	}
	defer cur.Close(context.Background())
	var quantity float64
	var results []bson.M
	if err = cur.All(context.TODO(), &results); err != nil {
		log.Fatal(err)
		return quantity, err
	}
	for _, result := range results {
		quantity += result["quantity"].(float64)
	}
	//Getting quantity from reserved_products table
	coll_rp := mongodb.Database("abl").Collection("reserved_products")
	filter1 := bson.D{{Key: "prd_id", Value: prd_id}}
	cur1, err1 := coll_rp.Find(context.Background(), filter1)
	if err1 != nil {
		log.Fatal(err1)
	}
	defer cur1.Close(context.Background())
	var results1 []bson.M
	if err = cur1.All(context.TODO(), &results1); err != nil {
		log.Fatal(err)
		return quantity, err
	}
	for _, result := range results1 {
		quantity -= result["quantity"].(float64)
	}
	return quantity, nil
}

func AddToCartcolection(UUID string, prd_id int, user_id int, quantity int, price int) (string, error) {

	db_quantity, _ := GetProductQtyDetails(UUID, prd_id)
	if int(db_quantity) < quantity {
		return "No Stock ", errors.New("Required Stock not available")
	}

	collection := mongodb.Database("abl").Collection("addtocart")
	result, err := collection.InsertOne(context.Background(), bson.D{
		{Key: "user_id", Value: user_id},
		{Key: "prd_id", Value: prd_id},
		{Key: "quantity", Value: quantity},
		{Key: "price", Value: price},
		{Key: "createdAt", Value: time.Now()},
	})
	if err != nil {
		log.Fatal(err)
		return "Failed", err
	}
	log.Printf("ReqID :%v result: %v", UUID, result)
	return "Products added to Cart successfully", nil
}

func GetProductCartDetails(UUID string, user_id int) ([]map[string]interface{}, error) {
	var rows []map[string]interface{}
	filter := bson.D{{Key: "user_id", Value: user_id}}
	collection := mongodb.Database("abl").Collection("addtocart")
	cur, err := collection.Find(context.Background(), filter)
	if err != nil {
		log.Fatal(err)
		return rows, errors.New("MongoDB error")
	}
	defer cur.Close(context.Background())

	var results []bson.M
	if err = cur.All(context.TODO(), &results); err != nil {
		log.Fatal(err)
		return rows, err
	}
	log.Printf("ReqID :%v User : %v addtocart results: %v", UUID, user_id, results)
	for _, result := range results {
		log.Printf("ReqID :%v User : %v addtocart rec: %v", UUID, user_id, result)
		row := make(map[string]interface{})
		row["user_id"] = result["user_id"].(int32)
		row["prd_id"] = result["prd_id"].(int32)
		row["quantity"] = result["quantity"].(int32)
		row["price"] = result["price"].(int32)
		rows = append(rows, row)
	}
	return rows, nil
}

//3rd party call for Payment Gateway
func MakePayment(UUID string, user_id int, price int32) (string, error) {
	log.Printf("ReqID :%v User : %v Payment request recieved: %v", UUID, user_id, price)
	log.Printf("ReqID :%v User : %v Payment done: %v", UUID, user_id, price)
	return "Ok", nil
}

//Add Products to Reserved Products table, to avoid extra orders
func AddtoReservedProducts(UUID string, inputdata []map[string]interface{}) (string, error) {
	log.Printf("ReqID :%v Adding to reserved_products: %v", UUID, inputdata)
	//Adding details to reserved_products table.
	//Once payment sucessfull then remove from the table or TTL Index created on this table, automatically remove after 5 mins
	collection := mongodb.Database("abl").Collection("reserved_products")
	var qdata []interface{}
	for _, rec := range inputdata {
		qdata = append(qdata, bson.D{
			{Key: "user_id", Value: rec["user_id"].(int32)},
			{Key: "prd_id", Value: rec["prd_id"].(int32)},
			{Key: "quantity", Value: rec["quantity"].(int32)},
			{Key: "price", Value: rec["price"].(int32)},
			{Key: "createdAt", Value: time.Now()},
		})
	}
	result, err := collection.InsertMany(context.Background(), qdata)
	if err != nil {
		log.Errorf("ReqID :%v Err: %v", UUID, err)
		return "Failed", err
	}
	log.Printf("ReqID :%v result: %v", UUID, result)
	return "OK", nil
}

//Remove Products from Cart and RP, once payement sucessfull
func RemoveProductsFromCartAndRP(UUID string, collection_name string, user_id int, prd_id []int) (string, error) {

	//var qdata []interface{}
	for _, rec := range prd_id {
		qdata := bson.M{
			"user_id": user_id, "prd_id": rec,
		}

		log.Printf("ReqID :%v user_id: %v qdata :%v", UUID, user_id, qdata)
		collection := mongodb.Database("abl").Collection(collection_name)
		result, err := collection.DeleteMany(context.Background(), qdata)
		if err != nil {
			log.Errorf("ReqID :%v user_id :%v delete %v err: %v", UUID, user_id, collection_name, err)
		}
		log.Printf("ReqID :%v removed from %v result: %v", UUID, collection_name, result)

	}
	return "OK", nil
}

func CheckoutCart(UUID string, user_id int) (string, error) {

	cart_data, _ := GetProductCartDetails(UUID, user_id)
	log.Printf("ReqID :%v user_id :%v cart_data :%v", UUID, user_id, cart_data)
	if len(cart_data) == 0 {
		return "No Cart details ", errors.New("No Products available in Cart")
	}

	var total_price int32
	var products []int
	for _, rec := range cart_data {
		price := rec["price"].(int32)
		quantity := rec["quantity"].(int32)
		prd_id := int(rec["prd_id"].(int32))
		//Re verify Products stock available
		curr_qty, _ := GetProductQtyDetails(UUID, prd_id)
		if quantity > int32(curr_qty) {
			return "Requested quantity not available", errors.New("Requested quantity not available")
		} else {
			log.Printf("ReqID :%v user_id :%v prd_id :%v current qty :%v requested qty :%v", UUID, user_id, prd_id, curr_qty, quantity)
		}
		total_price += quantity * price
		products = append(products, int(rec["prd_id"].(int32)))
	}
	log.Printf("ReqID :%v user_id :%v total_price :%v", UUID, user_id, total_price)
	//Adding to ReservedProducts to avoid inconsistent orders

	_, rp_err := AddtoReservedProducts(UUID, cart_data)
	if rp_err != nil {
		log.Fatal(rp_err)
		return "Failed to add ReservedProducts", errors.New("Products not added into ReservedProducts ")
	}

	// Making payment through 3rd party
	_, err := MakePayment(UUID, user_id, total_price)
	if err != nil {
		log.Fatal(err)
		//Remove reserved_products for unsuccessfull payments
		_, rm_err := RemoveProductsFromCartAndRP(UUID, "reserved_products", user_id, products)
		if rm_err != nil {
			log.Errorf("ReqID :%v user_id :%v err: %v", UUID, user_id, rm_err)
		}
		return "Payment Unsuccessfull", errors.New("Payment not done")
	}

	log.Printf("ReqID :%v user_id :%v updating quantity in product table :%v", UUID, user_id, cart_data)
	//Update Products Quantity
	coll_pr := mongodb.Database("abl").Collection("products")
	for _, rec := range cart_data {

		_, err_pr := coll_pr.UpdateOne(
			context.Background(),
			bson.M{"_id": int(rec["prd_id"].(int32))},
			bson.D{
				{Key: "$inc", Value: bson.D{{Key: "quantity", Value: -1 * int(rec["quantity"].(int32))}}},
			},
		)
		if err_pr != nil {
			log.Errorf("ReqID :%v user_id :%v err: %v", UUID, user_id, err)
			//Remove reserved_products for unsuccessfull payments
			_, rm_err := RemoveProductsFromCartAndRP(UUID, "reserved_products", user_id, products)
			if rm_err != nil {
				log.Errorf("ReqID :%v user_id :%v err: %v", UUID, user_id, rm_err)
			}
			return "update product quantity Unsuccessfull", errors.New("update product quantity Unsuccessfull")
		}
	}

	//Adding details to booked_orders table once payment sucessfull
	collection := mongodb.Database("abl").Collection("booked_orders")
	var inputdata []interface{}
	for _, rec := range cart_data {
		inputdata = append(inputdata, bson.D{
			{Key: "user_id", Value: int(rec["user_id"].(int32))},
			{Key: "prd_id", Value: int(rec["prd_id"].(int32))},
			{Key: "quantity", Value: int(rec["quantity"].(int32))},
			{Key: "price", Value: rec["price"].(int32)},
			{Key: "status", Value: "Order placed"},
			{Key: "createdAt", Value: time.Now()},
		})
	}
	result, err := collection.InsertMany(context.Background(), inputdata)
	if err != nil {
		log.Errorf("ReqID :%v user_id :%v err: %v", UUID, user_id, err)
		//Remove reserved_products for unsuccessfull transaction
		_, rm_err := RemoveProductsFromCartAndRP(UUID, "reserved_products", user_id, products)
		if rm_err != nil {
			log.Errorf("ReqID :%v user_id :%v err: %v", UUID, user_id, rm_err)
		}
		return "Adding Booked order failed", err
	}
	log.Printf("ReqID :%v added Booked order successfully, result: %v", UUID, result)

	//Remove Cart data and reserved_products once successfully processed
	_, rm_err := RemoveProductsFromCartAndRP(UUID, "reserved_products", user_id, products)
	if rm_err != nil {
		log.Errorf("ReqID :%v user_id :%v err: %v", UUID, user_id, rm_err)
		return "Failed to Remove Products from reserved_products", errors.New("Failed to remove Product from reserved_products")
	}
	_, rm_err = RemoveProductsFromCartAndRP(UUID, "addtocart", user_id, products)
	if rm_err != nil {
		log.Errorf("ReqID :%v user_id :%v err: %v", UUID, user_id, rm_err)
		return "Failed to Remove Products from addtocart", errors.New("Failed to remove Product from addtocart")
	}

	return "Checked out successfully", nil
}
