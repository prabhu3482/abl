package redis_conn

import (
	"errors"
	"fmt"
	"time"

	Config "../config"
	Utils "../utils"
	"github.com/gomodule/redigo/redis"
	log "github.com/Sirupsen/logrus"
)

var ARedisPool *redis.Pool
func init() {
	initRedisPool()
}

func initRedisPool() {
	var err error
	for i := 0; i <= Config.MAXRETRY; i++ {
		ARedisPool = NewPool(Config.AREDISHOST)
		_, err = ARedisPool.Dial()
		if err == nil {
			break
		}
		log.Fatalf("Could not connect to primary aredisserver at %s Error : %v", Config.AREDISHOST, err)
	}
}

//NewPool Get redis pool
func NewPool(server string) *redis.Pool {

	return &redis.Pool{
		MaxIdle:     100,
		IdleTimeout: 240 * time.Second,
		Wait:        true,
		MaxActive:   200,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", server)
			if err != nil {
				log.Fatalf("Couldnot connect to redis err:%v",err)
				return nil, err
			}
			return c, err
		},
	}
}

func PushRedis(RedisPool *redis.Pool, REDISHOST string, QUEUENAME string, data string) (string, error) {
	conn := RedisPool.Get()
	_, err := conn.Do("RPUSH", QUEUENAME, data)
	if err != nil {
		fmt.Println(err)
		log.Print(err)
		log.Printf("Could not rpush to redisserver at %s : RPUSH %s \"%s\" ", REDISHOST, QUEUENAME, data)
		return "failed", errors.New("Could not rpush to redisserver")
	}
	conn.Close()
	return "success", nil
}

func PushToRedis(QUEUENAME string, data string) (string, error){
	_, err := PushRedis(ARedisPool, Config.AREDISHOST, QUEUENAME, data)
	if err != nil {
		//Retrying
		_, err1 := PushRedis(ARedisPool, Config.BREDISHOST, QUEUENAME, data)
		if err1 != nil {
			return "failure", err1
		}
	}
	return "success", nil
}

func DoHget(Pool *redis.Pool, queueName string, field string) (string, bool) {
        conn := Pool.Get()
        value, err := conn.Do("HGET", queueName, field)
        conn.Close()
        if err != nil {
                return err.Error(), false
        } else if value == nil {
                return "0", false
        }
        return string(value.([]uint8)), true
}

func DoHIncrBy(Pool *redis.Pool, queueName string, field string, val int) (string, bool) {
        conn := Pool.Get()
        value, err := conn.Do("HINCRBY", queueName, field, val)
        conn.Close()
        if err != nil {
                return err.Error(), false
        } else if value == nil {
                return "0", false
        }

        return fmt.Sprint(value), true
}

