all:	clean goserver start apitest

clean:
	rm -vf apiserver

goserver:
	GOOS=linux go build src/apiserver.go src/routes.go

start:
	./apiserver &

srcstart:
	go run src/apiserver.go src/routes.go

apitest:
	go test test/main_test.go -v

install:
	sh install.sh
