#!/bin/sh

set -x 
echo "Installing Go Packages"
go get -u -v "github.com/valyala/fasthttp" "github.com/Sirupsen/logrus" "github.com/natefinch/lumberjack" "github.com/satori/go.uuid" "github.com/gomodule/redigo/redis"

echo "make.."
make

echo "make test..."
make test

echo "Done.."
